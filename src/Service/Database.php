<?php

namespace App\Service;

use PDO;

class Database {

    /**
     * @var String
     */
    private $host;

    /**
     * @var String
     */
    private $db_name;

    /**
     * @var String
     */
    private $user;

    /**
     * @var String
     */
    private $password;


    public function __construct($config)
    {
        $this->host = $config["host"];
        $this->db_name = $config["db_name"];
        $this->user = $config["user"];
        $this->password = $config["password"];
    }

    /**
     * Connection to the database
     * @return PDO
     */
    private function conn() {
        try{
            $connection = new PDO('mysql:host='. $this->host.';dbname='. $this->db_name, $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        }catch (\Exception $e) {
            exit('FATAL ERROR : Database connection failed');
        }
        return $connection;
    }

    /**
     * Querying in the database
     * @param $statement
     * @return array
     */
    private function querying($statement) {
        $result = array();
        foreach($this->conn()->query($statement) as $row) {
            array_push($result, $row);
        }
        return $result;
    }

    public function getOne($table, $condition) {
        $query = 'SELECT * FROM '.$table.' WHERE '.$condition['column'].' LIKE "%'.$condition['value'].'%" LIMIT 1';

        return $this->querying($query);
    }
}