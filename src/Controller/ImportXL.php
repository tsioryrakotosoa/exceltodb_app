<?php

namespace App\Controller;

use App\Service\Database;
use App\service\Personne;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Settings;
use App\Utils\Utils;

class ImportXL {

    /**
     * path to the concerned Excel file
     * @var String
     */
    private $PATH;

    /**
     * name of the concerned Excel file
     * @var mixed
     */
    private $excelFilename;

    /**
     * Instance of bdd connection
     * @var Object
     */
    private $bdd;

    /**
     * Instance of Person class
     * @var Object
     */
    private $person;

    /**
     * Name of the script to create
     * @var String
     */
    private $script_name;

    public function __construct($config, $arg)
    {
        if($this->isValidExpression($arg)) {
            $this->PATH = $config['path'];
            $this->excelFilename = $arg[2];
            $this->script_name = $arg[3];
        } else {
            exit("ERROR : THE RIGHT EXPRESSION SHOULD BE \"php index.php [XL-File-name]\"");
        }
        $this->bdd = new Database($config['database']);
    }

    /**
     * Check if the command is valid
     * @param $arguments
     * @return bool
     */
    public function isValidExpression($arguments) {
        if(!isset($arguments[1]) || !isset($arguments[2]) || !isset($arguments[3])) {
            return false;
        }
        return true;
    }

    /**
     * Get the path of concerned Excel file
     * @return string
     */
    public function getFile() {
        return $this->PATH.'\\'.$this->excelFilename;
    }

    /**
     * Write the SQL script
     */
    public function writeSqlScript() {
        $script_file = $this->PATH.'/'.$this->script_name;
        $script_open = fopen("$script_file", "w+");
        $persons = $this->getAllRows();
        foreach ($persons as $person) {
            fwrite($script_open, 'UPDATE personne SET personne_hebergement_situation_au_premier_jour = '.$person["hebFirstDay_1"].', personne_hebergement_situation_au_premier_jour_etape_2 = '.$person["hebFirstDay_2"].' WHERE numero_personne = '.$person["nPersonne"].';'.PHP_EOL);
        }
    }

    /**
     * Get all rows informations in the Excel files
     */
    public function getAllRows() {
        $excelrows = array();
        $file = $this->getFile();
        $object = IOFactory::load($file);
        $maxLine = $object->getActiveSheet()->getHighestRow();
        for ($i=2; $i<=$maxLine; $i++) {
            $arrayTemp['nPersonne'] = $object->getActiveSheet()->getCell('A'.$i)->getValue();
            $hebFirstDay_1 = $object->getActiveSheet()->getCell('F'.$i)->getValue();
            $state_condition = [
                "column" => Utils::TYPE_PERSONNE_HEBERGEMENT_SITUATION_AU_PREMIER_JOUR_COLUMN[1],
                "value" => $hebFirstDay_1
            ];
            $arrayTemp['hebFirstDay_1'] = (int)($this->bdd->getOne('type_personne_hebergement_situation_au_premier_jour', $state_condition)[0]->type_personne_hebergement_situation_au_premier_jour_id);
            $arrayTemp['hebFirstDay_2'] = ($object->getActiveSheet()->getCell('G'.$i)->getValue()!='-') ? '"'.$object->getActiveSheet()->getCell('G'.$i)->getValue().'"' : 'NULL';
            array_push($excelrows, $arrayTemp);
        }

        return $excelrows;
    }
}