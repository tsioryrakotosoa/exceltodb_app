<?php

use App\Controller\ImportXL;
use App\Utils\Helper;
use App\Service\Database;

require_once realpath("vendor/autoload.php");
require_once realpath("src/Utils/Helper.php");
require_once realpath("config.php");

//$db = new Database($_CONFIG['database']);
$import = new ImportXL($_CONFIG, $argv);
$import->writeSqlScript();

